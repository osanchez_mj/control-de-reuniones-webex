$('#busqueda').on('keyup', function () {
    var dato = $('#busqueda').val();
    var url = 'libraries/busqueda.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'dato=' + dato,

        success: function (datos) {
            $('#agrega-registros').html(datos);
        }
    });
    return false;
});
