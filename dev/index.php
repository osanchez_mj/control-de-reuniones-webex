<?php include 'config/config.php';?>
    <?php include 'libraries/database.php';?>
        <?php include 'libraries/consultas.php';?>
           <?php include 'libraries/helpers.php';?>
            <!DOCTYPE html>
            <html lang="es">

            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
                <meta name="Omar y Cris" content="Sitio de control de reuniones webex">
                <!--    <link rel="icon" href="../../favicon.ico"> Poner el de SCALA-->

                <title>Control Reuniones Webex</title>

                <!-- Bootstrap
                core CSS -->
                <!-- build:css styles/main.css -->
                <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
                <link rel="stylesheet" href="../dev/css/material.min.css">
                <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
                <link href="css/stilos.css" rel="stylesheet">

                  <!-- endbuild-->

                <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
                <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

                <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
                <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

            </head>

            <body>

                <div class="container-fluid">

                    <!-- Main component for a primary marketing message or call to action -->
                    <div class="jumbotron">
                        <div class="row">
                            <div class="col-xs-12">
                                <img class="img-responsive center-block" src="img/Logotipo-AC%20357x100.png" alt="Logo AC">
                                <h1 class="text-center">Control de Reuniones Webex</h1>
                                <p class="text-center">Mediante esta aplciación se llevara una bitacora de las reuniones webex que se realicen</p>

                            </div>
                        </div>
                        <div class="row">

                            <div class="col-xs-12 col-sm-4">
                                <!-- Div de reuniones del 2016 -->
                                <div class="demo-card-square mdl-card mdl-shadow--2dp">
                                    <div class="mdl-card__title mdl-card--expand">
                                        <h2 class="mdl-card__title-text">Reuniones en el 2016</h2>
                                    </div>
                                    <div class="mdl-card__supporting-text">
                                        <ul class="list-group">
                                            <li id="card-color" class="list-group-item active">
                                                <span class="badge"><?php echo $totalReuniones['total']?></span> Reuniones Totales
                                            </li>
                                            <?php while ($row= $totalLugar->fetch_assoc() ):?>
                                                <li class="list-group-item">
                                                    <span class="badge"><?php echo $row['total_lugar']?></span>
                                                    <?php echo $row['lugar']?>
                                                </li>
                                                <?php endwhile;?>
                                        </ul>
                                    </div>

                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4">
                                <!-- Square card -->
                                <div class="demo-card-square mdl-card mdl-shadow--2dp">
                                    <div class="mdl-card__title mdl-card--expand">
                                        <h2 class="mdl-card__title-text">Costo de las reuniones en 2016</h2>
                                    </div>
                                    <div class="mdl-card__supporting-text">
                                        <ul class="list-group">
                                            <li id="card-color" class="list-group-item active">
                                                <span class="badge">$ <?php echo $costoTotal['costo']?></span> Costo Total
                                            </li>
                                            <?php while ($row= $costoLugar->fetch_assoc() ):?>
                                                <li class="list-group-item">
                                                    <span class="badge">$ <?php echo $row['suma_costo']?></span>
                                                    <?php echo $row['lugar']?>
                                                </li>
                                                <?php endwhile;?>
                                        </ul>
                                    </div>

                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4">
                                <!-- Square card -->
                                <div class="demo-card-square mdl-card mdl-shadow--2dp">
                                    <div class="mdl-card__title mdl-card--expand">
                                        <h2 class="mdl-card__title-text">No. de participantes en 2016</h2>
                                    </div>
                                    <div class="mdl-card__supporting-text">
                                        <ul class="list-group">
                                            <li id="card-color" class="list-group-item active">
                                                <span class="badge"><?php echo $totalParticipantes['participantes']?></span> Total de participantes
                                            </li>
                                            <?php while ($row= $participantesLugar->fetch_assoc() ):?>
                                                <li class="list-group-item">
                                                    <span class="badge"><?php echo $row['total_participantes']?></span>
                                                    <?php echo $row['lugar']?>
                                                </li>
                                                <?php endwhile;?>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>


                </div>
                <!-- /container -->

                <div class="container">

                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="text-center"><i class="fa fa-bars"></i> Menú</h2>

                            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                                <div class="mdl-tabs__tab-bar">


                                    <a href="#add" class="mdl-tabs__tab is-active"><i class="icons-menu fa fa-calendar-plus-o"></i> Reuniones</a>
                                    <a href="#reportes" class="mdl-tabs__tab"><i class="icons-menu fa fa-line-chart"></i> Reportes</a>



                                </div>
                                <!-- ************************** TAB PANES  ************** -->

                                <div class="tab-content">
                                    <div class="reuniones">
                                        <div role="tabpanel" class="mdl-tabs__panel is-active" id="add">

                                            <form action="libraries/procesamiento.php" method="post">

                                                <div class="form-group">

                                                    <label for="exampleInputEmail1"><i class="iconos fa fa-calendar-o"></i> Nombre de la reunión</label>

                                                    <input name="reunion" type="text" class="capitalize form-control" placeholder="Ingresa el nombre que tendra la reunión" required>

                                                </div>

                                                <div class="form-group">

                                                    <label for="exampleInputPassword1"><i class="iconos fa fa-male"></i> Solicitante</label>
                                                    <input name="solicitante" type="text" class="capitalize form-control" placeholder="Nombre de la persona que solicita la reunión" required>

                                                </div>

                                                <div class="form-group">

                                                    <label for="exampleInputPassword1"><i class="iconos fa fa-globe"></i> Lugar</label>
                                                    <input name="lugar" type="text" class="capitalize form-control" placeholder="Lugar donde se relizará la reunión" required>

                                                </div>

                                                <div class="form-group">

                                                    <label for="exampleInputPassword1"><i class="iconos fa fa-calendar"></i> fecha</label>
                                                    <input name="fecha" type="date" class="form-control" required>

                                                </div>

                                                <div class="form-group">

                                                    <label for="exampleInputPassword1"><i class="iconos fa fa-users"></i> Número de Participantes</label>
                                                    <input name="numeroP" type="number" class="form-control" required>

                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1"><i class="iconos fa fa-envelope"></i> Correos de los participantes</label>

                                                    <textarea name="correos" class="form-control" rows="3" placeholder="correo de los participantes separados por comas" required></textarea>


                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1"><i class="iconos fa fa-user-plus"></i> Tecnico Asignado</label>


                                                    <select class="form-control" name="Tecnico" required>
                                                        <option value="Cristobal Bello Perez">Cristobal Bello Perez</option>
                                                        <option value="Omar Sánchez Martínez">Omar Sánchez Martínez</option>
                                                        <option value="Benjamin castillo dueñas">Benjamin castillo dueñas</option>
                                                        <option value="Flor Lázaro Romero">Flor Lázaro Romero</option>
                                                        <option value="Leilani de la Cruz Toledo">Leilani de la Cruz Toledo</option>
                                                        <option value="Alejandro de Jesus Osorno">Alejandro de Jesus Osorno</option>
                                                    </select>

                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputPassword1"><i class="iconos fa fa-money"></i> Costo</label>
                                                    <input name="precio" type="text" class="form-control" placeholder="Costo simulado de la reuníon" required>


                                                </div>



                                                <button type="submit" id="btn-color" class="btn btn-block mdl-button mdl-js-button mdl-button--raised mdl-js-r ipple-effect mdl-button--accent">Enviar</button>

                                            </form>


                                        </div>




                                        <?php if (isset($_GET['respuesta'])):?>

                                            <div id="alert-personal" class="alert alert-warning alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <?php echo $_GET['respuesta']?>
                                            </div>





                                            <?php endif;?>
                                    </div>


                                    <div role="tabpanel" class="mdl-tabs__panel" id="reportes">
                                        <div class="reportes">


                                                <form class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-xs-2 col-xs-offset-1 control-label"><i class="busqueda fa fa-search"></i></label>
                                                        <div class="col-xs-7">
                                                            <input type="text" class="form-control" id="busqueda" placeholder="Busca por reunion o solicitante o lugar o tecnico">
                                                        </div>
                                                        <div class="col-xs-2">
                                                        </div>
                                                    </div>
                                                </form>
                                                <div id="agrega-registros">

                                                    <div class="table-responsive">

                                                        <table class="table table-striped">

                                                            <tr id="btn-color">
                                                                <th class="col-r text-center"><i class="fa fa-calendar-o"></i> Reuni&oacute;n</th>
                                                                <th class="col-s text-center"><i class="fa fa-male"></i> Solicitante </th>
                                                                <th class="col-l text-center"><i class="fa fa-globe"></i> Lugar</th>
                                                                <th class="col-f text-center"><i class="fa fa-calendar"></i> Fecha</th>
                                                                <th class="col-p text-center"><i class="fa fa-users"></i> No. Participantes</th>
                                                                <th class="col-c text-center"><i class="fa fa-envelope"></i> Correos</th>
                                                                <th class="col-t text-center"><i class="fa fa-user-plus"></i> Tecnico</th>
                                                                <th class="col-cost text-center"><i class="fa fa-money"></i> Costo</th>
                                                            </tr>

                                                            <?php while ($row = $result->fetch_assoc()):?>

                                                                <tr>
                                                                    <td class="text-center">
                                                                        <?php echo $row['nombre_reunion']?>
                                                                    </td>
                                                                    <td class="text-center" id="fecha">
                                                                        <?php echo $row['solicitante']?>
                                                                    </td>
                                                                    <td class="text-center" id="fecha">
                                                                        <?php echo $row['lugar']?>
                                                                    </td>
                                                                    <td class="text-center" id="fecha">
                                                                        <?php echo formatoFecha($row['fecha'])?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <?php echo $row['num_participantes']?>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo shortenText($row['emails'])?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <?php echo $row['tecnico']?>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <?php echo '$ '.$row['costo']?>
                                                                    </td>
                                                                </tr>

                                                                <?php endwhile;?>
                                                        </table>
                                                    </div>
                                                </div>


                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <footer>

                    <p>copyright Administración del conocimiento, 2016</p>

                </footer>



                <!-- Bootstrap core JavaScript
    ================================================== -->
                <!-- Placed at the end of the document so the pages load faster -->
                <!-- build:js scripts/main.js -->
                <script src="../bower_components/jquery/dist/jquery.min.js"></script>
                <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
                <script src="../bower_components/material-design-lite/material.min.js"></script>
                <script src="js/ajax.js"></script>
            <!--endbuild-->
            </body>

            </html>
