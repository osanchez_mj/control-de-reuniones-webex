<?php
date_default_timezone_set('America/Mexico_City');
function formatoFecha($date){


setlocale(LC_TIME, 'spanish');
$fecha = utf8_encode(strftime("%A %d de %B del %y",strtotime($date)));
return $fecha;

}




function shortenText($text, $chars = 80) {

	$text= $text." ";
	//substr devuelve una parte de todo el texto que se le indique;
	$text=substr($text, 0, $chars);

	//Posteriormente volvemos a utilizar la misma función para que strrpos busque la ultima ocurrencia " " en este caso y nos regrese toda la cadena
	//y al final solo concatenamos los 3 puntos

	$text=substr($text, 0, strrpos($text, " "));
	$text = $text. " ...";


	return $text;



}

?>
