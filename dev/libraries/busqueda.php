
<?php include ('../config/config.php');?>
<?php include ('database.php');?>
<?php include 'helpers.php';?>
<?php

$dato = $_POST['dato']; //Esta dato lo obtiene de la funcion onkeyup en el ajax

//EJECUTAMOS LA CONSULTA DE BUSQUEDA

$db = new DataBase;

$query = "SELECT * FROM registro
WHERE nombre_reunion LIKE '%$dato%' OR solicitante LIKE '%$dato%'  OR tecnico LIKE '%$dato%'  OR lugar LIKE '%$dato%'
ORDER BY fecha DESC";

//$resultado
$busqueda=$db ->select($query);

//CREAMOS NUESTRA VISTA Y LA DEVOLVEMOS AL AJAX

if ($busqueda){

    echo '<div class="table-responsive">

                             <table class="table table-striped">

                                                            <tr id="btn-color">
                                                                <th class="col-r text-center"><i class="fa fa-calendar-o"></i> Reuni&oacute;n</th>
                                                                <th class="col-s text-center"><i class="fa fa-male"></i> Solicitante </th>
                                                                <th class="col-l text-center"><i class="fa fa-globe"></i> Lugar</th>
                                                                <th class="col-f text-center"><i class="fa fa-calendar"></i> Fecha</th>
                                                                <th class="col-p text-center"><i class="fa fa-users"></i> No. Participantes</th>
                                                                <th class="col-c text-center"><i class="fa fa-envelope"></i> Correos</th>
                                                                <th class="col-t text-center"><i class="fa fa-user-plus"></i> Tecnico</th>
                                                                <th class="col-cost text-center"><i class="fa fa-money"></i> Costo</th>
                                                            </tr>';
    while ($row=$busqueda->fetch_assoc()){
		echo ' <tr>
                                                                    <td class="text-center">
                                                                         '.$row['nombre_reunion'].'
                                                                    </td>
                                                                    <td class="text-center" id="fecha">
                                                                         '.$row['solicitante'].'
                                                                    </td>
                                                                    <td class="text-center" id="fecha">
                                                                         '.$row['lugar'].'
                                                                    </td>
                                                                    <td class="text-center" id="fecha">
                                                                         '.formatoFecha($row['fecha']).'
                                                                    </td>
                                                                    <td class="text-center">
                                                                         '.$row['num_participantes'].'
                                                                    </td>
                                                                    <td>
                                                                         '.shortenText($row['emails']).'
                                                                    </td>
                                                                    <td class="text-center">
                                                                         '.$row['tecnico'].'
                                                                    </td>
                                                                    <td class="text-center">
                                                                         '.$row['costo'].'
                                                                    </td>
                                                                </tr>';
	}

    echo '</table>
            </div>';




}


else{

    echo '<div class="alert alert-danger text-center" role="alert"><i class="fa fa-exclamation-triangle"></i>  No se encontraron resultados </div>';


}


?>
