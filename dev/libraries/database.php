<?php

class DataBase{

	private $host = DB_HOST;
	private $user = DB_USER;
	private $password = DB_PASS;
	private $dbName = DB_NAME;

	private $link;
	private $error;

	public function __construct(){

		$this->connect();

	}

	private function connect() {

		$this->link= new mysqli($this->host, $this->user, $this->password, $this->dbName);

		if (!$this->link) {
			$this->error="Error al conectar con la base de datos". $this->link->connec_error;
			return false;
		}

	}

	public function select($query) {
		$result=$this->link->query($query) or die ($this->link->error.__LINE__);

		if ($result->num_rows > 0) {
			return $result;
		}
		else{

			return false;
		}
	}

	public function insert($query) {
		$insertRow=$this->link->query($query) or die ($this->link->error.__LINE__);

		if (!$insertRow) {
			die("Error: ('. $this->link->errno . ')". $this->link->error );
		}

	}
}

?>
