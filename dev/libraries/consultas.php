<?php

$db= new DataBase;

//Query para mostrar todos los registros en la seccion de reportes
$query = "SELECT * FROM registro order by fecha DESC";

$result = $db->select($query);

//Query para obtener el total de reistros
$query = "SELECT count(*) as total FROM registro";

$totalReuniones = $db->select($query);
$totalReuniones = $totalReuniones->fetch_assoc();

//Query para obtener el total de costo de las reuniones
$query = "SELECT sum(costo) as costo FROM registro";

$costoTotal = $db->select($query);
$costoTotal = $costoTotal->fetch_assoc();

//Query para obtener el total de participantes en las reuniones
$query = "SELECT sum(num_participantes) as participantes FROM registro";

$totalParticipantes = $db->select($query);
$totalParticipantes = $totalParticipantes->fetch_assoc();

//query para agrupar por lugar y mostrar el numero de reuniones totales en cada lugar

$query = "SELECT lugar, count(lugar) AS total_lugar FROM reunines_webex.registro GROUP BY lugar;";

$totalLugar=$db->select($query);

//query para obtener la suma de los costos separados por lugar

$query = "SELECT lugar, sum(costo) as suma_costo FROM reunines_webex.registro GROUP BY lugar;";

$costoLugar = $db ->select($query);

//query para obtner el total de asistentes en cada reunion webex por lugar

$query = "SELECT lugar, sum(num_participantes) as total_participantes FROM reunines_webex.registro GROUP BY lugar";

$participantesLugar= $db->select($query);

?>
